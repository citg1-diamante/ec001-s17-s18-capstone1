package com.zuitt.capstone.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="user")
public class User {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String username;
    @Column
   // @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    //MAKE TO FROM ONE TO MANY -> MANY TO MANY
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<CourseEnrollment> enrollments;

    public User(){}

    public User(String username){this.username=username;}
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public int getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String Username){
        this.username = Username;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String Password){
        this.password = Password;
    }

    public Set<CourseEnrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(Set<CourseEnrollment> enrollments) {
        this.enrollments = enrollments;
    }
}
