package com.zuitt.capstone.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

//int id, naame, description, price, enrollees set, isActive - boolean
@Entity
@Table(name="course")
public class Course {

    @Id
    @GeneratedValue
    private int id;

    @Column
    private String name;

    @Column
    private String description;
    @Column
    private double price;


    @OneToMany(mappedBy="course")
    @JsonIgnore
    private Set<CourseEnrollment> enrollees;

    @Column
    private Boolean isActive;

    public Course(){}
    public Course(String name, String description, double price, Boolean isActive){
        this.name = name;
        this.description = description;
        this.price=price;
        this.isActive=isActive;
    }

    //Setters and Getters

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public double getPrice(){
        return price;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setDescription(String description){
        this.description=description;
    }

    public void setPrice(double price){
        this.price=price;
    }

    public Set<CourseEnrollment> getEnrollees() {
        return enrollees;
    }

    public void setEnrollees(Set<CourseEnrollment> enrollees) {
        this.enrollees = enrollees;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

}
