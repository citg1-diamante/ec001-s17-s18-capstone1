package com.zuitt.capstone.models;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.zuitt.capstone.models.User;

//id, courseId, userId, dateTimeEnrolled

@Entity
//@Table(name="courses_enrolled")
public class CourseEnrollment{
    @Id
    private int id;

    @ManyToOne
    @JoinColumn(name="user_id")
    User user;

    @ManyToOne
    @JoinColumn(name="course_id")
    Course course;

    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment() {
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getDateTimeEnrolled() {
        return dateTimeEnrolled;
    }

    public void setDateTimeEnrolled(LocalDateTime dateTimeEnrolled) {
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}
