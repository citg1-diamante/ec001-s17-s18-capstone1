package com.zuitt.capstone.repositories;

import com.zuitt.capstone.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Object> {

    Course findByName(String name);
}
