package com.zuitt.capstone.controllers;

import com.zuitt.capstone.exceptions.UserException;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.services.CourseService;
import com.zuitt.capstone.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;

    // Create courses
    @RequestMapping(value="/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourses(@RequestBody Course course) {
        courseService.createCourse(course);
        return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
    }

    // Get courses
    @RequestMapping(value="/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourses() {
        return new ResponseEntity<>(courseService.getCourses(), HttpStatus.OK);
    }

    // Delete courses
    @RequestMapping(value = "/courses/{courseid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteCourses(@PathVariable int courseid) {
        return courseService.deleteCourse(courseid);
    }

    // Update courses
    @RequestMapping(value="/courses/{courseid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateCourses(@PathVariable int courseid, @RequestBody Course course) {
        return courseService.updateCourse(courseid, course);
    }

    // Course Enroll
    @RequestMapping(value="/courses/create", method = RequestMethod.POST)
    // Register method takes a" request body as a Map of key-value pairs", where the keys are strings and the values are strings. It also throws a "UserException" in case of an error, which is a "custom exception".
    public ResponseEntity<Object> register(@RequestBody Map<String, String> body) throws UserException {
        // This retrieves the value associated with the "username" key from the request body "Map" and assigns it to a String variable called "username".
        String name = body.get("name");

        // check if the user provided "username" exists in the database, if the user exists, it throws a UserException with the message "Username already exists."
        if (!courseService.findByCourse(name).isEmpty()) {
            throw new UserException("Course already exists.");
        }
        // if username doesn't exists, it will proceed on creating of the client.
        else {

            String description = body.get("description");
            double price = Double.parseDouble(body.get("price"));
            Boolean isActive = Boolean.valueOf(body.get("isActive"));

            // Instantiates the Course model to create a new course
            Course newCourse = new Course(name, description, price, isActive);

            // saves in the "newCourse" in the database.
            courseService.createCourse(newCourse);

            // Sends a "Course created successfully" message as the response body and an HTTP status code of 201.
            return new ResponseEntity<>("Course created successfully", HttpStatus.CREATED);
        }
    }

}
