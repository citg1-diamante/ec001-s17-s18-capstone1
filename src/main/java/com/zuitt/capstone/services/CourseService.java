package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface CourseService {

    void createCourse(Course course);

    Iterable<Course> getCourses();
    ResponseEntity deleteCourse(int id);

    ResponseEntity updateCourse(int id, Course course);

    //    Optional - defines if the method may/may not return an object of the User Class
    Optional<Course> findByCourse(String name);

}
